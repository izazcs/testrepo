<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <title> Live Tennis Matches Data Scrapping</title>
  <link rel="shortcut icon" href="http://localhost/kpos/assets/favicon.png" type="image/png">
  <link rel="icon" type="image/png" href="http://localhost/kpos/assets/favicon.png">
  <!--Bootstrap-->

  <link rel="stylesheet" type="text/css" href="assets/klabo/css/bootstrap.css">
  <!--Stylesheets-->
  <link rel="stylesheet" type="text/css" href="assets/klabo/css/style.css">
  <!--Responsive-->
  <link rel="stylesheet" type="text/css" href="assets/klabo/css/responsive.css">
  <!--Animation-->
  <link rel="stylesheet" type="text/css" href="assets/klabo/css/animate.css">
  <!--Prettyphoto-->
  <link rel="stylesheet" type="text/css" href="assets/klabo/css/prettyPhoto.css">
  <!--Font-Awesome-->
  <link rel="stylesheet" type="text/css" href="assets/klabo/css/font-awesome.css">
  <!--Owl-Slider-->
  <link rel="stylesheet" type="text/css" href="assets/klabo/css/owl.carousel.css">
  <link rel="stylesheet" type="text/css" href="assets/klabo/css/owl.theme.css">
  <link rel="stylesheet" type="text/css" href="assets/klabo/css/owl.transitions.css">
</head>

<body data-spy="scroll" data-target=".navbar-default" data-offset="100">
  <!--Preloader-->
  <div id="preloader">
    <div id="pre-status">
      <div class="preload-placeholder"></div>
    </div>
  </div>
  <!--Navigation-->
  <header id="menu">
    <div class="navbar navbar-default navbar-fixed-top" style="background-color: #0099ff;">
      <div class="container">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand" href="#menu"><img src="assets/img/klab.png" width="80px" alt=""></a>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="active"><a class="scroll" href="#menu">HOME</a></li>
              <li><a class="scroll" href="#">Login</a></li>
            </ul>
          </div>
          <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
      </div>
    </div>
  </header>
 
  <!--Section for form generation-->
  <section id="team" style="margin-top: 30px;">
    <div class="container">
      <h2>SMS with GSM</h2>
      <h5>Changes made with pull request form cloudways</h5>
      <div class="row">
        <!-- [ horizontal-layout ] start -->
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="ipport">IP and PORT</label>
                            <input type="text" class="form-control" value="http://192.168.10.3:7272" name="ipport" id="ipport"  placeholder="ip and port.">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="number">Mobile Number</label>
                            <input type="text" class="form-control" name="number" value="00923459514672" id="number">
                        </div>
                        <div class="form-group col-md-4">
                          <label for="user">User</label>
                          <input type="text" class="form-control" value="khan" name="user"  id="user">
                      </div>
                      <div class="form-group col-md-4">
                        <label for="pass">Password</label>
                        <input type="text" class="form-control" value="khan" name="pass"  id="pass">
                      </div>
                        <div class="form-group col-md-4">
                            <label for="message">Message</label>
                            <textarea name="message" id="message" class="form-control" cols="20" rows="5"> Test Message </textarea>
                        </div>
                       
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                        </div>
                        <div class="form-group col-md-6">
                            <button type="submit" class="btn btn-primary has-ripple" id="btnsendsms" name="submit"><i class="feather mr-2 icon-check-circle"></i>SEND SMS</button>
                            <button type="reset" class="btn btn-dark has-ripple"> <i class="feather mr-2 icon-check-circle"></i>RESET</button>
                        </div>
                    </div>
                  
                </div>
            </div>
        </div>
        <!-- [ horizontal-layout ] end -->
    </div>
  </section>
  <!--/Section for form generation-->


  <!--Contact-Section-Start-->
  <!-- [ Main Content ] end -->
  <footer id="footer">
    <div class="bg-sec" style="background-color: #4da6ff;">
      <div class="container">
        <h2>LOOKING FORWARD TO <strong>HEARING </strong>FROM YOU!</h2>
      </div>
    </div>
  </footer>
  <footer id="footer-down">
    <h2>Follow Us On</h2>
    <ul class="social-icon">
      <li class="facebook hvr-pulse"><a href=""><i class="fa fa-facebook-f"></i></a></li>
      <li class="twitter hvr-pulse"><a href=""><i class="fa fa-twitter"></i></a></li>
      <li class="youtube hvr-pulse"><a href=""><i class="fa fa-youtube"></i></a></li>
    </ul>
    <section id="contact">
      <div class="container">
        <div class="col-md-8 col-md-offset-2">
          <div class="text-center">
            <h4>CONTACT IN<span>FO</span></h4>
            <ul class="contact-form">
              <li><i class="fa fa-map-marker"></i>
                <h6><strong>Address:</strong> </h6>
              </li>
              <li><i class="fa fa-envelope"></i>
                <h6><strong>Mail Us:</strong> <a href="#"></a></h6>
              </li>
              <li><i class="fa fa-phone"></i>
                <h6><strong>Phone:</strong> </h6>
              </li>
            </ul>
          </div>

    </section>
    <p> &copy; Copyright 2021 <a href="http://www.digigofast.com" target="_blank">DIGIGOFAST</a> </p>
  </footer>
  <!--Jquery-->
  <script type="text/javascript" src="assets/klabo/js/jquery.min.js"></script>
  <!--Boostrap-Jquery-->
  <script type="text/javascript" src="assets/klabo/js/bootstrap.js"></script>
  <!--Preetyphoto-Jquery-->
  <script type="text/javascript" src="assets/klabo/js/jquery.prettyPhoto.js"></script>
  <!--NiceScroll-Jquery-->
  <script type="text/javascript" src="assets/klabo/js/jquery.nicescroll.js"></script>
  <!--Isotopes-->
  <script type="text/javascript" src="assets/klabo/js/jquery.isotope.js"></script>
  <!--Wow-Jquery-->
  <script type="text/javascript" src="assets/klabo/js/wow.js"></script>
  <!--Count-Jquey-->
  <script type="text/javascript" src="assets/klabo/js/jquery.countTo.js"></script>
  <script type="text/javascript" src="assets/klabo/js/jquery.inview.min.js"></script>
  <!--Owl-Crousels-Jqury-->
  <script type="text/javascript" src="assets/klabo/js/owl.carousel.js"></script>
  <!--Main-Scripts-->
  <script type="text/javascript" src="assets/klabo/js/script.js"></script>
</body>
<script>
  $(document).ready(function(){
    $('#btnsendsms').click(function(){
      var ipport = $('#ipport').val();
      var number = $('#number').val();
      var user = $('#user').val();
      var pass = $('#pass').val();
      var message = $('#message').val();
      //alert(ipport + number+ user+pass+message);
      $.ajax({
				url: ipport+'/SendSMS?username='+user+'&password='+pass+'&phone='+number+'&message='+message,
				data: {},
				type: 'POST',
				success: function(data){
					console.log(data);
          console.log("SMS Sended Successfully");
				},
				error: function(){
					console.log('Error found!');
				}
			});
    });
  });

</script>

</html>